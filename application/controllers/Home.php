<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	var $API = '';

	function __construct()
	{
		parent::__construct();
		$this->API  = "https://api.kawalcorona.com/indonesia/";
		$this->API2 = "https://api.kawalcorona.com/";
	}

	public function index()
	{
		$data['corona'] = json_decode($this->curl->simple_get($this->API));
		$data['allcorona'] = json_decode($this->curl->simple_get($this->API2));

		$this->load->view('home', $data);
	}

}
