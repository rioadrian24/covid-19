<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>COVID 19</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?= base_url('assets/css/mdb.min.css') ?>" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
</head>

<body>

  <!-- Navbar -->
  <nav class="navbar navbar-dark default-color">
    <span class="navbar-brand font-weight-bold">COVID 19 <small>BY RIO ADRIAN</small></span>
  </nav>
  <!-- End Navbar -->

  <!-- Card -->
  <div class="card card-cascade wider reverse mb-3">

    <!-- Card image -->
    <div class="view view-cascade overlay">
      <img class="card-img-top" src="https://myips.org/wp-content/uploads/2020/03/COVID_19N__Web_Banner.jpg"
      alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!-- Card content -->
    <div class="card-body card-body-cascade text-center">
      <!-- Title -->
      <h5 class="card-title"><strong>DATA CORONA DI INDONESIA</strong></h5>      
    </div>

  </div>
  <!-- Card -->

  <!-- Card data -->
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mb-3">
        <div class="card testimonial-card mt-2 mb-4">

          <!-- Background color -->
          <div class="card-up purple-gradient"></div>

          <!-- Avatar -->
          <div class="avatar mx-auto white">
            <img src="https://www.netclipart.com/pp/m/30-302494_facebook-sad-emoji-png-facebook-sad-icon-png.png" class="rounded-circle img-responsive">
          </div>

          <!-- Content -->
          <div class="card-body">
            <!-- Name -->
            <h4 class="card-title font-weight-bold">POSITIF</h4>
            <hr>
            <!-- Quotation -->
            <h2><?= number_format($corona[0]->positif) ?> Orang</h2>
          </div>

        </div>
      </div>

      <div class="col-lg-4 mb-3">
        <div class="card testimonial-card mt-2 mb-4">

          <!-- Background color -->
          <div class="card-up blue-gradient"></div>

          <!-- Avatar -->
          <div class="avatar mx-auto white">
            <img src="https://thumbs.dreamstime.com/b/smile-vector-icon-happy-smiling-face-emoticon-icon-flat-style-yellow-symbol-white-background-smile-vector-icon-happy-150159497.jpg" class="rounded-circle img-responsive">
          </div>

          <!-- Content -->
          <div class="card-body">
            <!-- Name -->
            <h4 class="card-title font-weight-bold">SEMBUH</h4>
            <hr>
            <!-- Quotation -->
            <h2><?= number_format($corona[0]->sembuh) ?> Orang</h2>
          </div>

        </div>
      </div>

      <div class="col-lg-4 mb-3">
        <div class="card testimonial-card mt-2 mb-4">

          <!-- Background color -->
          <div class="card-up peach-gradient"></div>

          <!-- Avatar -->
          <div class="avatar mx-auto white">
            <img src="https://i0.pngocean.com/files/624/979/696/crying-expression.jpg" class="rounded-circle img-responsive">
          </div>

          <!-- Content -->
          <div class="card-body">
            <!-- Name -->
            <h4 class="card-title font-weight-bold">MENINGGAL</h4>
            <hr>
            <!-- Quotation -->
            <h2><?= number_format($corona[0]->meninggal) ?> Orang</h2>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- End Card data -->

  <!-- Card -->
  <div class="card card-cascade wider reverse mb-4">
    <!-- Card content -->
    <div class="card-body card-body-cascade text-center">
      <!-- Title -->
      <h5 class="card-title"><strong>DATA CORONA DI SELURUH DUNIA</strong></h5>      
    </div>
  </div>
  <!-- Card -->

  <!-- All Data -->
  <div class="container">
    <div class="row">
      <?php foreach ($allcorona as $all): ?>
        <div class="col-lg-4 mb-4">
          <div class="card">
            <div class="card-body">
              <h4 class="text-center"><?= $all->attributes->Country_Region ?></h4> <hr>
              <p>Positif&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= number_format($all->attributes->Confirmed) ?></p>
              <p>Sembuh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= number_format($all->attributes->Recovered) ?></p>
              <p>Meninggal : <?= number_format($all->attributes->Deaths) ?></p>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>
  </div>
  <!-- End All Data -->

  <!-- Footer -->
  <footer class="py-5 shadow">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          Copyright &copy; 2020 COVID 19 MADE WITH <i class="fas fa-heart text-danger"></i> Rio Adrian Maulana
        </div>
      </div>
    </div>
  </footer>
  <!-- End Footer -->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?= base_url('assets/js/popper.min.js') ?>"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?= base_url('assets/js/mdb.min.js') ?>"></script>
</body>

</html>